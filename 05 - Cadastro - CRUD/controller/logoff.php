<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logoff</title>
    </head>
    <body>
        <?php
        session_start();
        session_destroy();
        header("location: ../view/index.php");
        ?>
    </body>
</html>