<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        include 'padrao.php';
        ?>
        <title>BLR Rede Social</title>
    </head>
    <body>
        <h4>Bem-Vindo!</h4><br>
        <?php
        //sem utilizar estilos, apenas links
        session_start();
        if (!isset($_SESSION['login'])) {
            ?>
            <a href="login.php">Login</a><br>
            <a href="cadastroUsuario.php">Cadastre-se</a>
            <?php
        }
        else
        {
            echo 'Matrícula: '.$_SESSION['login'];
            ?>
            <br><a href="../controller/logoff.php">Logoff</a><br>
            <a href="editaUsuario.php">Meu Perfil</a><br>
            <a href="listaContatos.php">Meus Contatos</a>
            <?php 
        }
        ?>
    </body>
</html>
