-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 04-Maio-2018 às 22:33
-- Versão do servidor: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `t1_alfa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `artistas_musicais`
--

DROP TABLE IF EXISTS `artistas_musicais`;
CREATE TABLE IF NOT EXISTS `artistas_musicais` (
  `uria` varchar(100) NOT NULL,
  PRIMARY KEY (`uria`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `artistas_musicais`
--

INSERT INTO `artistas_musicais` (`uria`) VALUES
('https://en.wikipedia.org/wiki/65daysofstatic'),
('https://en.wikipedia.org/wiki/AAA_(band)'),
('https://en.wikipedia.org/wiki/AC/DC'),
('https://en.wikipedia.org/wiki/Adrian_von_Ziegler'),
('https://en.wikipedia.org/wiki/Aerosmith'),
('https://en.wikipedia.org/wiki/Ai_Maeda_(voice_actress)'),
('https://en.wikipedia.org/wiki/Alan_Jackson'),
('https://en.wikipedia.org/wiki/Anavit%C3%B3ria'),
('https://en.wikipedia.org/wiki/Andy_McKee'),
('https://en.wikipedia.org/wiki/Andy_Timmons'),
('https://en.wikipedia.org/wiki/Animals_as_Leaders'),
('https://en.wikipedia.org/wiki/Arctic_Monkeys'),
('https://en.wikipedia.org/wiki/Armin_van_Buuren'),
('https://en.wikipedia.org/wiki/Asian_Kung-Fu_Generation'),
('https://en.wikipedia.org/wiki/Avenged_Sevenfold'),
('https://en.wikipedia.org/wiki/A_Day_to_Remember'),
('https://en.wikipedia.org/wiki/Big_Gigantic'),
('https://en.wikipedia.org/wiki/Black_Label_Society'),
('https://en.wikipedia.org/wiki/Black_Sabbath'),
('https://en.wikipedia.org/wiki/Blake_Shelton'),
('https://en.wikipedia.org/wiki/Blue_%C3%96yster_Cult'),
('https://en.wikipedia.org/wiki/Bon_Jovi'),
('https://en.wikipedia.org/wiki/Brockhampton_(band)'),
('https://en.wikipedia.org/wiki/Bruno_Mars'),
('https://en.wikipedia.org/wiki/BTS_(band)'),
('https://en.wikipedia.org/wiki/Burzum'),
('https://en.wikipedia.org/wiki/C%C5%93ur_de_pirate'),
('https://en.wikipedia.org/wiki/Camisa_de_V%C3%AAnus'),
('https://en.wikipedia.org/wiki/Caravan_Palace'),
('https://en.wikipedia.org/wiki/Carla_Bruni'),
('https://en.wikipedia.org/wiki/Cartola'),
('https://en.wikipedia.org/wiki/Celldweller'),
('https://en.wikipedia.org/wiki/Charlie_Brown_Jr.'),
('https://en.wikipedia.org/wiki/Coldplay'),
('https://en.wikipedia.org/wiki/Cro_(singer)'),
('https://en.wikipedia.org/wiki/Czecho_No_Republic'),
('https://en.wikipedia.org/wiki/Darren_Korb'),
('https://en.wikipedia.org/wiki/David_Arnold'),
('https://en.wikipedia.org/wiki/David_Bowie'),
('https://en.wikipedia.org/wiki/Day6'),
('https://en.wikipedia.org/wiki/Deadmau5'),
('https://en.wikipedia.org/wiki/Deep_Purple'),
('https://en.wikipedia.org/wiki/Die_Toten_Hosen'),
('https://en.wikipedia.org/wiki/Dimitri_Vegas_%26_Like_Mike'),
('https://en.wikipedia.org/wiki/Dire_Straits'),
('https://en.wikipedia.org/wiki/Disturbed_(band)'),
('https://en.wikipedia.org/wiki/Drake_(musician)'),
('https://en.wikipedia.org/wiki/Dropkick_Murphys'),
('https://en.wikipedia.org/wiki/Dua_Lipa'),
('https://en.wikipedia.org/wiki/Eagles_(band)'),
('https://en.wikipedia.org/wiki/Eazy-E'),
('https://en.wikipedia.org/wiki/Ed_Sheeran'),
('https://en.wikipedia.org/wiki/Elvis_Presley'),
('https://en.wikipedia.org/wiki/Eminem'),
('https://en.wikipedia.org/wiki/Engenheiros_do_Hawaii'),
('https://en.wikipedia.org/wiki/Eric_Clapton'),
('https://en.wikipedia.org/wiki/Evanescence'),
('https://en.wikipedia.org/wiki/Exile_(Japanese_band)'),
('https://en.wikipedia.org/wiki/Exo_(band)'),
('https://en.wikipedia.org/wiki/Explosions_in_the_Sky'),
('https://en.wikipedia.org/wiki/Fall_Out_Boy'),
('https://en.wikipedia.org/wiki/Five_Finger_Death_Punch'),
('https://en.wikipedia.org/wiki/Flume_(musician)'),
('https://en.wikipedia.org/wiki/Foo_Fighters'),
('https://en.wikipedia.org/wiki/Frank_Sinatra'),
('https://en.wikipedia.org/wiki/Godspeed_You!_Black_Emperor'),
('https://en.wikipedia.org/wiki/Gorillaz'),
('https://en.wikipedia.org/wiki/Guns_N%27_Roses'),
('https://en.wikipedia.org/wiki/Hans_Zimmer'),
('https://en.wikipedia.org/wiki/Hardwell'),
('https://en.wikipedia.org/wiki/Hillsong_United'),
('https://en.wikipedia.org/wiki/Howard_Shore'),
('https://en.wikipedia.org/wiki/Imagine_Dragons'),
('https://en.wikipedia.org/wiki/Iron_maiden'),
('https://en.wikipedia.org/wiki/Jack_Johnson_(musician)'),
('https://en.wikipedia.org/wiki/Jacob_Collier'),
('https://en.wikipedia.org/wiki/Jeremy_Soule'),
('https://en.wikipedia.org/wiki/Jesus_Culture'),
('https://en.wikipedia.org/wiki/Joe_Satriani'),
('https://en.wikipedia.org/wiki/Johnny_Cash'),
('https://en.wikipedia.org/wiki/John_Mayer'),
('https://en.wikipedia.org/wiki/John_Williams'),
('https://en.wikipedia.org/wiki/Jonti_Picking'),
('https://en.wikipedia.org/wiki/Jon_Lajoie'),
('https://en.wikipedia.org/wiki/Joss_Stone'),
('https://en.wikipedia.org/wiki/Joy_Division'),
('https://en.wikipedia.org/wiki/Justin_Timberlake'),
('https://en.wikipedia.org/wiki/J_Balvin'),
('https://en.wikipedia.org/wiki/K.Will'),
('https://en.wikipedia.org/wiki/K?ji_Wada'),
('https://en.wikipedia.org/wiki/Kaleo_(band)'),
('https://en.wikipedia.org/wiki/Kendrick_Lamar'),
('https://en.wikipedia.org/wiki/Keyakizaka46'),
('https://en.wikipedia.org/wiki/Kiko_Loureiro'),
('https://en.wikipedia.org/wiki/Korn'),
('https://en.wikipedia.org/wiki/Led_Zeppelin'),
('https://en.wikipedia.org/wiki/Legi%C3%A3o_Urbana'),
('https://en.wikipedia.org/wiki/Legião_Urbana'),
('https://en.wikipedia.org/wiki/Limp_Bizkit'),
('https://en.wikipedia.org/wiki/Lincoln_Brewster'),
('https://en.wikipedia.org/wiki/Linkin_Park'),
('https://en.wikipedia.org/wiki/Lorde'),
('https://en.wikipedia.org/wiki/Madeon'),
('https://en.wikipedia.org/wiki/Maroon_5'),
('https://en.wikipedia.org/wiki/Martin_Garrix'),
('https://en.wikipedia.org/wiki/Massacration'),
('https://en.wikipedia.org/wiki/Matanza_(band)'),
('https://en.wikipedia.org/wiki/MC_Carol'),
('https://en.wikipedia.org/wiki/Metallica'),
('https://en.wikipedia.org/wiki/MGMT'),
('https://en.wikipedia.org/wiki/Michael_W._Smith'),
('https://en.wikipedia.org/wiki/Michel_Tel%C3%B3'),
('https://en.wikipedia.org/wiki/Molejo'),
('https://en.wikipedia.org/wiki/Mot%C3%B6rhead'),
('https://en.wikipedia.org/wiki/Mrs._Green_Apple'),
('https://en.wikipedia.org/wiki/Mumford_%26_Sons'),
('https://en.wikipedia.org/wiki/Muse_(band)'),
('https://en.wikipedia.org/wiki/Negoto'),
('https://en.wikipedia.org/wiki/Nickelback'),
('https://en.wikipedia.org/wiki/Nightcore'),
('https://en.wikipedia.org/wiki/Nirvana_(band)'),
('https://en.wikipedia.org/wiki/Odesza'),
('https://en.wikipedia.org/wiki/Oficina_G3'),
('https://en.wikipedia.org/wiki/OneRepublic'),
('https://en.wikipedia.org/wiki/Os_Paralamas_do_Sucesso'),
('https://en.wikipedia.org/wiki/Paramore'),
('https://en.wikipedia.org/wiki/Parov_Stelar'),
('https://en.wikipedia.org/wiki/Perturbator'),
('https://en.wikipedia.org/wiki/Pink_Floyd'),
('https://en.wikipedia.org/wiki/Plebe_Rude'),
('https://en.wikipedia.org/wiki/Postmodern_Jukebox'),
('https://en.wikipedia.org/wiki/Queen'),
('https://en.wikipedia.org/wiki/Queens_of_the_Stone_Age'),
('https://en.wikipedia.org/wiki/Queen_(band)'),
('https://en.wikipedia.org/wiki/Ra%C3%A7a_Negra'),
('https://en.wikipedia.org/wiki/Racionais_MC%27s'),
('https://en.wikipedia.org/wiki/Rammstein'),
('https://en.wikipedia.org/wiki/Ratatat'),
('https://en.wikipedia.org/wiki/Raul_Seixas'),
('https://en.wikipedia.org/wiki/Red'),
('https://en.wikipedia.org/wiki/Red_Hot_Chili_Peppers'),
('https://en.wikipedia.org/wiki/Reol_(band)'),
('https://en.wikipedia.org/wiki/Rica_Matsumoto'),
('https://en.wikipedia.org/wiki/Rüfüs'),
('https://en.wikipedia.org/wiki/Ryo_Fukui'),
('https://en.wikipedia.org/wiki/Sabaton_(band)'),
('https://en.wikipedia.org/wiki/Scandal_(Japanese_band)'),
('https://en.wikipedia.org/wiki/SDP_(band)'),
('https://en.wikipedia.org/wiki/Seafret'),
('https://en.wikipedia.org/wiki/Seventeen_(band)'),
('https://en.wikipedia.org/wiki/Shawn_Mendes'),
('https://en.wikipedia.org/wiki/Sido_(rapper)'),
('https://en.wikipedia.org/wiki/Sigur_R%C3%B3s'),
('https://en.wikipedia.org/wiki/Skank_(band)'),
('https://en.wikipedia.org/wiki/Skrillex'),
('https://en.wikipedia.org/wiki/Slipknot_(band)'),
('https://en.wikipedia.org/wiki/Snarky_Puppy'),
('https://en.wikipedia.org/wiki/SoundTeMP'),
('https://en.wikipedia.org/wiki/Stone_Sour'),
('https://en.wikipedia.org/wiki/Sungha_Jung'),
('https://en.wikipedia.org/wiki/SuperHeavy'),
('https://en.wikipedia.org/wiki/Superorganism_(band)'),
('https://en.wikipedia.org/wiki/System_of_a_Down'),
('https://en.wikipedia.org/wiki/Thee_Silver_Mt._Zion_Memorial_Orchestra'),
('https://en.wikipedia.org/wiki/The_Beatles'),
('https://en.wikipedia.org/wiki/The_Black_Keys'),
('https://en.wikipedia.org/wiki/The_Fratellis'),
('https://en.wikipedia.org/wiki/The_Killers'),
('https://en.wikipedia.org/wiki/The_Kooks'),
('https://en.wikipedia.org/wiki/The_Offspring'),
('https://en.wikipedia.org/wiki/The_Rolling_Stones'),
('https://en.wikipedia.org/wiki/The_Script'),
('https://en.wikipedia.org/wiki/The_Smiths'),
('https://en.wikipedia.org/wiki/The_Strokes'),
('https://en.wikipedia.org/wiki/The_Weeknd'),
('https://en.wikipedia.org/wiki/The_White_Stripes'),
('https://en.wikipedia.org/wiki/Thirty_Seconds_to_Mars'),
('https://en.wikipedia.org/wiki/Ti%C3%ABsto'),
('https://en.wikipedia.org/wiki/Tim_Maia'),
('https://en.wikipedia.org/wiki/Tommy_Emmanuel'),
('https://en.wikipedia.org/wiki/Tribalistas'),
('https://en.wikipedia.org/wiki/Twenty_One_Pilots'),
('https://en.wikipedia.org/wiki/Two_Steps_from_Hell'),
('https://en.wikipedia.org/wiki/Underoath'),
('https://en.wikipedia.org/wiki/Vintage_Culture'),
('https://en.wikipedia.org/wiki/Vocaloid'),
('https://en.wikipedia.org/wiki/W-inds'),
('https://en.wikipedia.org/wiki/Waterflame'),
('https://en.wikipedia.org/wiki/Weather_Report'),
('https://en.wikipedia.org/wiki/Wesley_Safad%C3%A3o'),
('https://en.wikipedia.org/wiki/Willie_Nelson'),
('https://en.wikipedia.org/wiki/Yoko_Kanno'),
('https://en.wikipedia.org/wiki/Zez%C3%A9_Di_Camargo_%26_Luciano');

-- --------------------------------------------------------

--
-- Stand-in structure for view `conhecenormalizada`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `conhecenormalizada`;
CREATE TABLE IF NOT EXISTS `conhecenormalizada` (
`login` varchar(100)
,`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

DROP TABLE IF EXISTS `contato`;
CREATE TABLE IF NOT EXISTS `contato` (
  `login` varchar(100) NOT NULL,
  `login2` varchar(100) NOT NULL,
  PRIMARY KEY (`login`,`login2`),
  KEY `contato_login2_fkey` (`login2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`login`, `login2`) VALUES
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801henriquecastro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiosordi', 'http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiosordi', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801hamiltonneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801hamiltonneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801henriquecastro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801henriquecastro', 'http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', 'http://utfpr.edu.br/CSB30/2018/1/DI1801hamiltonneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiosordi'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech', 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801hamiltonneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiosordi'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva', 'http://utfpr.edu.br/CSB30/2018/1/DI1801henriquecastro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `filmes`
--

DROP TABLE IF EXISTS `filmes`;
CREATE TABLE IF NOT EXISTS `filmes` (
  `urif` varchar(100) NOT NULL,
  PRIMARY KEY (`urif`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `filmes`
--

INSERT INTO `filmes` (`urif`) VALUES
('http://www.imdb.com/title/tt0033467/'),
('http://www.imdb.com/title/tt0050083/'),
('http://www.imdb.com/title/tt0050825/'),
('http://www.imdb.com/title/tt0052357/'),
('http://www.imdb.com/title/tt0054215/'),
('http://www.imdb.com/title/tt0057012/'),
('http://www.imdb.com/title/tt0060196/'),
('http://www.imdb.com/title/tt0062622/'),
('http://www.imdb.com/title/tt0066921/'),
('http://www.imdb.com/title/tt0068646/'),
('http://www.imdb.com/title/tt0070047/'),
('http://www.imdb.com/title/tt0070511/'),
('http://www.imdb.com/title/tt0071562/'),
('http://www.imdb.com/title/tt0071853/'),
('http://www.imdb.com/title/tt0075148/'),
('http://www.imdb.com/title/tt0075314/'),
('http://www.imdb.com/title/tt0076759/'),
('http://www.imdb.com/title/tt0077416/'),
('http://www.imdb.com/title/tt0078748/'),
('http://www.imdb.com/title/tt0080684/'),
('http://www.imdb.com/title/tt0083658/'),
('http://www.imdb.com/title/tt0086190/'),
('http://www.imdb.com/title/tt0086250/'),
('http://www.imdb.com/title/tt0086879/'),
('http://www.imdb.com/title/tt0088763/'),
('http://www.imdb.com/title/tt0090605/'),
('http://www.imdb.com/title/tt0093058/'),
('http://www.imdb.com/title/tt0095016/'),
('http://www.imdb.com/title/tt0095765/'),
('http://www.imdb.com/title/tt0096283/'),
('http://www.imdb.com/title/tt0097576/'),
('http://www.imdb.com/title/tt0099685/'),
('http://www.imdb.com/title/tt0102926/'),
('http://www.imdb.com/title/tt0103064/'),
('http://www.imdb.com/title/tt0107290/'),
('http://www.imdb.com/title/tt0108052/'),
('http://www.imdb.com/title/tt0109830/'),
('http://www.imdb.com/title/tt0110357/'),
('http://www.imdb.com/title/tt0110413/'),
('http://www.imdb.com/title/tt0110912/'),
('http://www.imdb.com/title/tt0111161/'),
('http://www.imdb.com/title/tt0112573/'),
('http://www.imdb.com/title/tt0114369/'),
('http://www.imdb.com/title/tt0114709/'),
('http://www.imdb.com/title/tt0114814/'),
('http://www.imdb.com/title/tt0120382/'),
('http://www.imdb.com/title/tt0120586/'),
('http://www.imdb.com/title/tt0120689/'),
('http://www.imdb.com/title/tt0120737/'),
('http://www.imdb.com/title/tt0133093/'),
('http://www.imdb.com/title/tt0137523/'),
('http://www.imdb.com/title/tt0167260/'),
('http://www.imdb.com/title/tt0167261/'),
('http://www.imdb.com/title/tt0167404/'),
('http://www.imdb.com/title/tt0172495/'),
('http://www.imdb.com/title/tt0198781/'),
('http://www.imdb.com/title/tt0209144/'),
('http://www.imdb.com/title/tt0211915/'),
('http://www.imdb.com/title/tt0245429/'),
('http://www.imdb.com/title/tt0246578/'),
('http://www.imdb.com/title/tt0253474/'),
('http://www.imdb.com/title/tt0266543/'),
('http://www.imdb.com/title/tt0266697/'),
('http://www.imdb.com/title/tt0317248/'),
('http://www.imdb.com/title/tt0319061/'),
('http://www.imdb.com/title/tt0325980/'),
('http://www.imdb.com/title/tt0338013/'),
('http://www.imdb.com/title/tt0361748/'),
('http://www.imdb.com/title/tt0372784/'),
('http://www.imdb.com/title/tt0382932/'),
('http://www.imdb.com/title/tt0401792/'),
('http://www.imdb.com/title/tt0405094/'),
('http://www.imdb.com/title/tt0434409/'),
('http://www.imdb.com/title/tt0435761/'),
('http://www.imdb.com/title/tt0440963/'),
('http://www.imdb.com/title/tt0454876/'),
('http://www.imdb.com/title/tt0468569/'),
('http://www.imdb.com/title/tt0482571/'),
('http://www.imdb.com/title/tt0758758/'),
('http://www.imdb.com/title/tt0796366/'),
('http://www.imdb.com/title/tt0848228/'),
('http://www.imdb.com/title/tt0892769/'),
('http://www.imdb.com/title/tt0903624/'),
('http://www.imdb.com/title/tt0910970/'),
('http://www.imdb.com/title/tt1010048/'),
('http://www.imdb.com/title/tt1049413/'),
('http://www.imdb.com/title/tt1201607/'),
('http://www.imdb.com/title/tt1205489/'),
('http://www.imdb.com/title/tt1220719/'),
('http://www.imdb.com/title/tt1345836/'),
('http://www.imdb.com/title/tt1375666/'),
('http://www.imdb.com/title/tt1659337/'),
('http://www.imdb.com/title/tt1675434/'),
('http://www.imdb.com/title/tt1853728/');

-- --------------------------------------------------------

--
-- Stand-in structure for view `ge1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `ge1`;
CREATE TABLE IF NOT EXISTS `ge1` (
`login` varchar(100)
,`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ge2`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `ge2`;
CREATE TABLE IF NOT EXISTS `ge2` (
`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `gostaartista`
--

DROP TABLE IF EXISTS `gostaartista`;
CREATE TABLE IF NOT EXISTS `gostaartista` (
  `idgostaartista` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `avaliacao` varchar(100) NOT NULL,
  `idart` varchar(100) NOT NULL,
  PRIMARY KEY (`idgostaartista`)
) ENGINE=MyISAM AUTO_INCREMENT=807 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `gostaartista`
--

INSERT INTO `gostaartista` (`idgostaartista`, `login`, `avaliacao`, `idart`) VALUES
(774, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'https://en.wikipedia.org/wiki/Wesley_Safad%C3%A3o'),
(773, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'https://en.wikipedia.org/wiki/J_Balvin'),
(772, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'https://en.wikipedia.org/wiki/AC/DC'),
(771, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'https://en.wikipedia.org/wiki/MC_Carol'),
(770, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'https://en.wikipedia.org/wiki/Vintage_Culture'),
(769, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'https://en.wikipedia.org/wiki/Racionais_MC%27s'),
(768, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'https://en.wikipedia.org/wiki/Alan_Jackson'),
(767, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'https://en.wikipedia.org/wiki/Eazy-E'),
(766, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'https://en.wikipedia.org/wiki/Mot%C3%B6rhead'),
(765, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'https://en.wikipedia.org/wiki/The_Beatles'),
(764, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '4', 'https://en.wikipedia.org/wiki/Coldplay'),
(763, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '4', 'https://en.wikipedia.org/wiki/Twenty_One_Pilots'),
(762, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '5', 'https://en.wikipedia.org/wiki/Imagine_Dragons'),
(761, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Cro_(singer)'),
(760, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Underoath'),
(759, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Thirty_Seconds_to_Mars'),
(758, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Slipknot_(band)'),
(757, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Seafret'),
(756, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/SDP_(band)'),
(755, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/Red'),
(754, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'https://en.wikipedia.org/wiki/A_Day_to_Remember'),
(753, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'https://en.wikipedia.org/wiki/Howard_Shore'),
(752, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'https://en.wikipedia.org/wiki/Waterflame'),
(751, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'https://en.wikipedia.org/wiki/John_Williams'),
(750, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'https://en.wikipedia.org/wiki/Jeremy_Soule'),
(749, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'https://en.wikipedia.org/wiki/David_Arnold'),
(748, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'https://en.wikipedia.org/wiki/Adrian_von_Ziegler'),
(747, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'https://en.wikipedia.org/wiki/Elvis_Presley'),
(746, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'https://en.wikipedia.org/wiki/The_Rolling_Stones'),
(745, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'https://en.wikipedia.org/wiki/Eagles_(band)'),
(744, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'https://en.wikipedia.org/wiki/Iron_Maiden'),
(743, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'https://en.wikipedia.org/wiki/Deep_Purple'),
(742, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'https://en.wikipedia.org/wiki/Bon_Jovi'),
(741, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'https://en.wikipedia.org/wiki/AC/DC'),
(740, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'https://en.wikipedia.org/wiki/Eminem'),
(739, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '4', 'https://en.wikipedia.org/wiki/Blake_Shelton'),
(738, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '2', 'https://en.wikipedia.org/wiki/Frank_Sinatra'),
(737, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'https://en.wikipedia.org/wiki/Charlie_Brown_Jr.'),
(736, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'https://en.wikipedia.org/wiki/Foo_Fighters'),
(735, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '5', 'https://en.wikipedia.org/wiki/Jack_Johnson_(musician)'),
(734, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '5', 'https://en.wikipedia.org/wiki/Molejo'),
(733, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '5', 'https://en.wikipedia.org/wiki/Ra%C3%A7a_Negra'),
(732, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'https://en.wikipedia.org/wiki/Linkin_Park'),
(731, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'https://en.wikipedia.org/wiki/Jon_Lajoie'),
(730, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '2', 'https://en.wikipedia.org/wiki/Reol_(band)'),
(729, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'https://en.wikipedia.org/wiki/Jonti_Picking'),
(728, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '4', 'https://en.wikipedia.org/wiki/Yoko_Kanno'),
(727, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'https://en.wikipedia.org/wiki/Jacob_Collier'),
(726, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'https://en.wikipedia.org/wiki/SoundTeMP'),
(725, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '4', 'https://en.wikipedia.org/wiki/Darren_Korb'),
(724, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'https://en.wikipedia.org/wiki/Snarky_Puppy'),
(723, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '4', 'https://en.wikipedia.org/wiki/Animals_as_Leaders'),
(722, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '4', 'https://en.wikipedia.org/wiki/Perturbator'),
(721, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'https://en.wikipedia.org/wiki/Ti%C3%ABsto'),
(720, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'https://en.wikipedia.org/wiki/Hardwell'),
(719, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'https://en.wikipedia.org/wiki/Armin_van_Buuren'),
(718, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'https://en.wikipedia.org/wiki/Dimitri_Vegas_%26_Like_Mike'),
(717, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'https://en.wikipedia.org/wiki/Martin_Garrix'),
(716, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'https://en.wikipedia.org/wiki/AC/DC'),
(715, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'https://en.wikipedia.org/wiki/Queen_(band)'),
(714, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'https://en.wikipedia.org/wiki/K.Will'),
(713, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'https://en.wikipedia.org/wiki/OneRepublic'),
(712, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'https://en.wikipedia.org/wiki/The_Script'),
(711, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'https://en.wikipedia.org/wiki/Shawn_Mendes'),
(710, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '5', 'https://en.wikipedia.org/wiki/Maroon_5'),
(709, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '5', 'https://en.wikipedia.org/wiki/Ed_Sheeran'),
(708, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'https://en.wikipedia.org/wiki/Coldplay'),
(707, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '3', 'https://en.wikipedia.org/wiki/Eminem'),
(706, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'https://en.wikipedia.org/wiki/Mumford_%26_Sons'),
(705, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'https://en.wikipedia.org/wiki/Kaleo_(band)'),
(704, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '4', 'https://en.wikipedia.org/wiki/Flume_(musician)'),
(703, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'https://en.wikipedia.org/wiki/Twenty_One_Pilots'),
(702, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'https://en.wikipedia.org/wiki/Metallica'),
(701, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'https://en.wikipedia.org/wiki/The_Strokes'),
(700, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '4', 'https://en.wikipedia.org/wiki/Paramore'),
(699, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/Maroon_5'),
(698, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/Justin_Timberlake'),
(697, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/John_Mayer'),
(696, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/Jesus_Culture'),
(695, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/Hillsong_United'),
(694, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '4', 'https://en.wikipedia.org/wiki/Foo_Fighters'),
(693, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '4', 'https://en.wikipedia.org/wiki/Coldplay'),
(692, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'https://en.wikipedia.org/wiki/Bruno_Mars'),
(691, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'https://en.wikipedia.org/wiki/Sido_(rapper)'),
(690, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Brockhampton_(band)'),
(689, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'https://en.wikipedia.org/wiki/Thee_Silver_Mt._Zion_Memorial_Orchestra'),
(688, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '1', 'https://en.wikipedia.org/wiki/Ed_Sheeran'),
(687, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '2', 'https://en.wikipedia.org/wiki/The_Kooks'),
(686, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '1', 'https://en.wikipedia.org/wiki/Michel_Tel%C3%B3'),
(685, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Johnny_Cash'),
(684, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'https://en.wikipedia.org/wiki/MGMT'),
(683, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/The_Smiths'),
(682, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(681, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Pink_Floyd'),
(680, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Godspeed_You!_Black_Emperor'),
(679, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Black_Sabbath'),
(678, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Joy_Division'),
(677, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'https://en.wikipedia.org/wiki/Die_Toten_Hosen'),
(676, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'https://en.wikipedia.org/wiki/Burzum'),
(675, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'https://en.wikipedia.org/wiki/Sigur_R%C3%B3s'),
(674, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '4', 'https://en.wikipedia.org/wiki/Queens_of_the_Stone_Age'),
(673, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '1', 'https://en.wikipedia.org/wiki/Vocaloid'),
(672, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'https://en.wikipedia.org/wiki/Skank_(band)'),
(671, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '4', 'https://en.wikipedia.org/wiki/Os_Paralamas_do_Sucesso'),
(670, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '4', 'https://en.wikipedia.org/wiki/Gorillaz'),
(669, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'https://en.wikipedia.org/wiki/Foo_Fighters'),
(668, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '3', 'https://en.wikipedia.org/wiki/Fall_Out_Boy'),
(667, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '5', 'https://en.wikipedia.org/wiki/Gorillaz'),
(666, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'https://en.wikipedia.org/wiki/Imagine_Dragons'),
(665, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'https://en.wikipedia.org/wiki/Asian_Kung-Fu_Generation'),
(664, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'https://en.wikipedia.org/wiki/System_of_a_Down'),
(663, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'https://en.wikipedia.org/wiki/The_Offspring'),
(662, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'https://en.wikipedia.org/wiki/Red_Hot_Chili_Peppers'),
(661, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '5', 'https://en.wikipedia.org/wiki/Linkin_Park'),
(660, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '5', 'https://en.wikipedia.org/wiki/Blue_%C3%96yster_Cult'),
(659, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(658, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/Queen'),
(657, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '3', 'https://en.wikipedia.org/wiki/David_Bowie'),
(656, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '4', 'https://en.wikipedia.org/wiki/Dire_Straits'),
(655, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/John_Williams'),
(654, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/Hans_Zimmer'),
(653, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '4', 'https://en.wikipedia.org/wiki/Kendrick_Lamar'),
(652, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '4', 'https://en.wikipedia.org/wiki/The_Weeknd'),
(651, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/Drake_(musician)'),
(650, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'https://en.wikipedia.org/wiki/Metallica'),
(649, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '4', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(648, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '4', 'https://en.wikipedia.org/wiki/The_Rolling_Stones'),
(647, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '5', 'https://en.wikipedia.org/wiki/The_Beatles'),
(646, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '4', 'https://en.wikipedia.org/wiki/Eric_Clapton'),
(645, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Cartola'),
(644, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '4', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(643, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Black_Sabbath'),
(642, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Dire_Straits'),
(641, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Pink_Floyd'),
(640, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Legi%C3%A3o_Urbana'),
(639, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'https://en.wikipedia.org/wiki/Tim_Maia'),
(638, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/Deadmau5'),
(637, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/Madeon'),
(636, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '4', 'https://en.wikipedia.org/wiki/Dua_Lipa'),
(635, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/Big_Gigantic'),
(634, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/Superorganism_(band)'),
(633, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/The_Killers'),
(632, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/The_Fratellis'),
(631, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/The_White_Stripes'),
(630, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/Arctic_Monkeys'),
(629, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'https://en.wikipedia.org/wiki/The_Strokes'),
(628, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'https://en.wikipedia.org/wiki/Plebe_Rude'),
(627, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'https://en.wikipedia.org/wiki/Camisa_de_V%C3%AAnus'),
(626, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '3', 'https://en.wikipedia.org/wiki/Matanza_(band)'),
(625, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'https://en.wikipedia.org/wiki/Guns_N%27_Roses'),
(624, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '5', 'https://en.wikipedia.org/wiki/Two_Steps_from_Hell'),
(623, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'https://en.wikipedia.org/wiki/Queen_(band)'),
(622, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'https://en.wikipedia.org/wiki/Nightcore'),
(621, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '5', 'https://en.wikipedia.org/wiki/Willie_Nelson'),
(620, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '5', 'https://en.wikipedia.org/wiki/AC/DC'),
(619, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '5', 'https://en.wikipedia.org/wiki/Johnny_Cash'),
(618, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/W-inds'),
(617, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '3', 'https://en.wikipedia.org/wiki/Seventeen_(band)'),
(616, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'https://en.wikipedia.org/wiki/Scandal_(Japanese_band)'),
(615, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/Rica_Matsumoto'),
(614, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'https://en.wikipedia.org/wiki/Negoto'),
(613, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '3', 'https://en.wikipedia.org/wiki/Mrs._Green_Apple'),
(612, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/K?ji_Wada'),
(611, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/Keyakizaka46'),
(610, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '2', 'https://en.wikipedia.org/wiki/Exo_(band)'),
(609, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '3', 'https://en.wikipedia.org/wiki/Exile_(Japanese_band)'),
(608, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/Day6'),
(607, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'https://en.wikipedia.org/wiki/Czecho_No_Republic'),
(606, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '3', 'https://en.wikipedia.org/wiki/BTS_(band)'),
(605, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/Ai_Maeda_(voice_actress)'),
(604, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'https://en.wikipedia.org/wiki/AAA_(band)'),
(603, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'https://en.wikipedia.org/wiki/Sungha_Jung'),
(602, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '3', 'https://en.wikipedia.org/wiki/Evanescence'),
(601, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '3', 'https://en.wikipedia.org/wiki/Lorde'),
(600, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '3', 'https://en.wikipedia.org/wiki/Zez%C3%A9_Di_Camargo_%26_Luciano'),
(599, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '4', 'https://en.wikipedia.org/wiki/Carla_Bruni'),
(598, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'https://en.wikipedia.org/wiki/Tommy_Emmanuel'),
(597, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'https://en.wikipedia.org/wiki/Andy_McKee'),
(596, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'https://en.wikipedia.org/wiki/Nirvana_(band)'),
(595, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '3', 'https://en.wikipedia.org/wiki/Metallica'),
(594, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '4', 'https://en.wikipedia.org/wiki/Weather_Report'),
(593, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '3', 'https://en.wikipedia.org/wiki/The_Black_Keys'),
(592, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'https://en.wikipedia.org/wiki/Ryo_Fukui'),
(591, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '3', 'https://en.wikipedia.org/wiki/Muse_(band)'),
(590, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(589, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '4', 'https://en.wikipedia.org/wiki/Joss_Stone'),
(588, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'https://en.wikipedia.org/wiki/Nickelback'),
(587, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'https://en.wikipedia.org/wiki/Legião_Urbana'),
(586, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'https://en.wikipedia.org/wiki/Metallica'),
(585, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'https://en.wikipedia.org/wiki/Celldweller'),
(584, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'https://en.wikipedia.org/wiki/Queens_of_the_Stone_Age'),
(583, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'https://en.wikipedia.org/wiki/Queen_(band)'),
(582, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'https://en.wikipedia.org/wiki/Avenged_Sevenfold'),
(581, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Engenheiros_do_Hawaii'),
(580, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '4', 'https://en.wikipedia.org/wiki/Raul_Seixas'),
(579, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Skrillex'),
(578, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Black_Sabbath'),
(577, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '4', 'https://en.wikipedia.org/wiki/Red_Hot_Chili_Peppers'),
(576, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Iron_Maiden'),
(575, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '4', 'https://en.wikipedia.org/wiki/Massacration'),
(574, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Rammstein'),
(573, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/Queen_(band)'),
(572, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'https://en.wikipedia.org/wiki/AC/DC'),
(571, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '4', 'https://en.wikipedia.org/wiki/Stone_Sour'),
(570, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '4', 'https://en.wikipedia.org/wiki/Iron_maiden'),
(569, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '4', 'https://en.wikipedia.org/wiki/Black_Label_Society'),
(568, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'https://en.wikipedia.org/wiki/Queen_(band)'),
(567, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '4', 'https://en.wikipedia.org/wiki/Metallica'),
(566, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'https://en.wikipedia.org/wiki/Frank_Sinatra'),
(565, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'https://en.wikipedia.org/wiki/Five_Finger_Death_Punch'),
(564, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'https://en.wikipedia.org/wiki/Disturbed_(band)'),
(563, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'https://en.wikipedia.org/wiki/SuperHeavy'),
(562, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'https://en.wikipedia.org/wiki/Limp_Bizkit'),
(561, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'https://en.wikipedia.org/wiki/Korn'),
(560, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '3', 'https://en.wikipedia.org/wiki/Dropkick_Murphys'),
(559, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '3', 'https://en.wikipedia.org/wiki/C%C5%93ur_de_pirate'),
(558, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '3', 'https://en.wikipedia.org/wiki/Tribalistas'),
(557, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'https://en.wikipedia.org/wiki/Aerosmith'),
(556, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'https://en.wikipedia.org/wiki/Joss_Stone'),
(555, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'https://en.wikipedia.org/wiki/Caravan_Palace'),
(554, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'https://en.wikipedia.org/wiki/Anavit%C3%B3ria'),
(553, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'https://en.wikipedia.org/wiki/Eminem'),
(552, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Odesza'),
(551, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/65daysofstatic'),
(550, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Postmodern_Jukebox'),
(549, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Ratatat'),
(548, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Rüfüs'),
(547, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Parov_Stelar'),
(546, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'https://en.wikipedia.org/wiki/Explosions_in_the_Sky'),
(545, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '5', 'https://en.wikipedia.org/wiki/Led_Zeppelin'),
(544, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '5', 'https://en.wikipedia.org/wiki/The_Fratellis'),
(543, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '5', 'https://en.wikipedia.org/wiki/Red_Hot_Chili_Peppers'),
(542, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '3', 'https://en.wikipedia.org/wiki/Lincoln_Brewster'),
(541, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '2', 'https://en.wikipedia.org/wiki/Michael_W._Smith'),
(540, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '4', 'https://en.wikipedia.org/wiki/Andy_Timmons'),
(539, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '5', 'https://en.wikipedia.org/wiki/Oficina_G3'),
(538, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '4', 'https://en.wikipedia.org/wiki/Kiko_Loureiro'),
(537, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '5', 'https://en.wikipedia.org/wiki/Joe_Satriani'),
(775, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://en.wikipedia.org/wiki/Nirvana_(band)'),
(776, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://en.wikipedia.org/wiki/Metallica'),
(777, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://en.wikipedia.org/wiki/Linkin_Park'),
(778, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '', 'https://en.wikipedia.org/wiki/Legião_Urbana'),
(779, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'https://en.wikipedia.org/wiki/Queen'),
(780, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '3', 'https://en.wikipedia.org/wiki/Bon_Jovi'),
(781, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'https://en.wikipedia.org/wiki/Aerosmith'),
(782, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://en.wikipedia.org/wiki/AC/DC'),
(783, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://en.wikipedia.org/wiki/Massacration'),
(784, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '1', 'https://en.wikipedia.org/wiki/MC_Carol'),
(785, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://en.wikipedia.org/wiki/Metallica'),
(786, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://en.wikipedia.org/wiki/Avenged_Sevenfold'),
(787, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '3', 'https://en.wikipedia.org/wiki/Eric_Clapton'),
(788, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://en.wikipedia.org/wiki/Coldplay'),
(789, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://en.wikipedia.org/wiki/AC/DC'),
(790, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://en.wikipedia.org/wiki/Sabaton_(band)'),
(791, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '', ''),
(792, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'https://en.wikipedia.org/wiki/Massacration'),
(793, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '1', 'https://en.wikipedia.org/wiki/MC_Carol'),
(794, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://en.wikipedia.org/wiki/Metallica'),
(795, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'https://en.wikipedia.org/wiki/Nirvana_(band)'),
(796, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://en.wikipedia.org/wiki/Linkin_Park'),
(797, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://en.wikipedia.org/wiki/Queen'),
(798, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://en.wikipedia.org/wiki/Yoko_Kanno'),
(799, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'https://en.wikipedia.org/wiki/Racionais_MC%27s'),
(800, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '2', 'https://en.wikipedia.org/wiki/Ai_Maeda_(voice_actress)'),
(801, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '3', 'https://en.wikipedia.org/wiki/Tim_Maia'),
(802, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'https://en.wikipedia.org/wiki/Racionais_MC%27s'),
(803, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '5', 'https://en.wikipedia.org/wiki/Paramore'),
(804, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '3', 'https://en.wikipedia.org/wiki/MC_Carol'),
(805, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '2', 'https://en.wikipedia.org/wiki/Jesus_Culture'),
(806, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '2', 'https://en.wikipedia.org/wiki/Gorillaz');

-- --------------------------------------------------------

--
-- Estrutura da tabela `gostafilme`
--

DROP TABLE IF EXISTS `gostafilme`;
CREATE TABLE IF NOT EXISTS `gostafilme` (
  `idgostafilme` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `avaliacao` varchar(100) NOT NULL,
  `idart` varchar(100) NOT NULL,
  PRIMARY KEY (`idgostafilme`)
) ENGINE=MyISAM AUTO_INCREMENT=669 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `gostafilme`
--

INSERT INTO `gostafilme` (`idgostafilme`, `login`, `avaliacao`, `idart`) VALUES
(634, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'http://www.imdb.com/title/tt0468569/'),
(633, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'http://www.imdb.com/title/tt0111161/'),
(632, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'http://www.imdb.com/title/tt0077416/'),
(629, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '2', 'http://www.imdb.com/title/tt0097576/'),
(631, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '3', 'http://www.imdb.com/title/tt0107290/'),
(630, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'http://www.imdb.com/title/tt0848228/'),
(628, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'http://www.imdb.com/title/tt0435761/'),
(627, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '4', 'http://www.imdb.com/title/tt0434409/'),
(626, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '3', 'http://www.imdb.com/title/tt0133093/'),
(625, 'http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', '5', 'http://www.imdb.com/title/tt0068646/'),
(622, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '3', 'http://www.imdb.com/title/tt1201607/'),
(624, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '4', 'http://www.imdb.com/title/tt0097576/'),
(623, 'http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', '5', 'http://www.imdb.com/title/tt0382932/'),
(621, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0102926/'),
(620, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0114369/'),
(619, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt1375666/'),
(618, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0137523/'),
(617, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0468569/'),
(616, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0110912/'),
(615, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '4', 'http://www.imdb.com/title/tt0068646/'),
(614, 'http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', '5', 'http://www.imdb.com/title/tt0111161/'),
(613, 'http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva', '3', 'http://www.imdb.com/title/tt0910970/'),
(612, 'http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva', '5', 'http://www.imdb.com/title/tt1375666/'),
(611, 'http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva', '4', 'http://www.imdb.com/title/tt0435761/'),
(610, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt1345836/'),
(609, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '3', 'http://www.imdb.com/title/tt0910970/'),
(608, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0468569/'),
(607, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'http://www.imdb.com/title/tt0372784/'),
(606, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0266543/'),
(605, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0198781/'),
(604, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'http://www.imdb.com/title/tt0167261/'),
(603, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'http://www.imdb.com/title/tt0167260/'),
(602, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'http://www.imdb.com/title/tt0120737/'),
(601, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0114709/'),
(600, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '5', 'http://www.imdb.com/title/tt0086190/'),
(599, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '3', 'http://www.imdb.com/title/tt0083658/'),
(598, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0080684/'),
(597, 'http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', '4', 'http://www.imdb.com/title/tt0076759/'),
(596, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'http://www.imdb.com/title/tt0848228/'),
(595, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'http://www.imdb.com/title/tt0103064/'),
(594, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'http://www.imdb.com/title/tt0137523/'),
(593, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '4', 'http://www.imdb.com/title/tt0110912/'),
(592, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'http://www.imdb.com/title/tt0109830/'),
(591, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'http://www.imdb.com/title/tt0075148/'),
(590, 'http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', '5', 'http://www.imdb.com/title/tt0068646/'),
(589, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '5', 'http://www.imdb.com/title/tt0361748/'),
(588, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'http://www.imdb.com/title/tt0172495/'),
(587, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'http://www.imdb.com/title/tt0050825/'),
(586, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'http://www.imdb.com/title/tt0057012/'),
(585, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '5', 'http://www.imdb.com/title/tt1853728/'),
(584, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '4', 'http://www.imdb.com/title/tt0317248/'),
(583, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '3', 'http://www.imdb.com/title/tt0109830/'),
(582, 'http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', '2', 'http://www.imdb.com/title/tt0468569/'),
(581, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'http://www.imdb.com/title/tt0468569/'),
(580, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'http://www.imdb.com/title/tt0071853/'),
(579, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'http://www.imdb.com/title/tt1205489/'),
(578, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'http://www.imdb.com/title/tt0266543/'),
(577, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'http://www.imdb.com/title/tt0892769/'),
(576, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '2', 'http://www.imdb.com/title/tt0903624/'),
(575, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '5', 'http://www.imdb.com/title/tt1010048/'),
(574, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'http://www.imdb.com/title/tt0070047/'),
(573, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '4', 'http://www.imdb.com/title/tt0120382/'),
(572, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '3', 'http://www.imdb.com/title/tt0382932/'),
(571, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '2', 'http://www.imdb.com/title/tt1201607/'),
(570, 'http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', '2', 'http://www.imdb.com/title/tt0796366/'),
(569, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'http://www.imdb.com/title/tt0209144/'),
(568, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'http://www.imdb.com/title/tt0078748/'),
(567, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'http://www.imdb.com/title/tt0088763/'),
(566, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'http://www.imdb.com/title/tt0133093/'),
(565, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'http://www.imdb.com/title/tt0076759/'),
(564, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '4', 'http://www.imdb.com/title/tt1375666/'),
(563, 'http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', '5', 'http://www.imdb.com/title/tt0080684/'),
(562, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '4', 'http://www.imdb.com/title/tt0910970/'),
(561, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '3', 'http://www.imdb.com/title/tt1375666/'),
(560, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '4', 'http://www.imdb.com/title/tt0910970/'),
(559, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '4', 'http://www.imdb.com/title/tt0848228/'),
(558, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '4', 'http://www.imdb.com/title/tt0133093/'),
(557, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '3', 'http://www.imdb.com/title/tt0088763/'),
(556, 'http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', '3', 'http://www.imdb.com/title/tt0076759/'),
(555, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'http://www.imdb.com/title/tt0892769/'),
(554, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'http://www.imdb.com/title/tt0903624/'),
(553, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '5', 'http://www.imdb.com/title/tt1375666/'),
(552, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'http://www.imdb.com/title/tt0211915/'),
(551, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '5', 'http://www.imdb.com/title/tt1201607/'),
(550, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'http://www.imdb.com/title/tt0068646/'),
(549, 'http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', '4', 'http://www.imdb.com/title/tt0910970/'),
(548, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '4', 'http://www.imdb.com/title/tt0083658/'),
(547, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '3', 'http://www.imdb.com/title/tt0372784/'),
(546, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0172495/'),
(545, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt1853728/'),
(544, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0099685/'),
(543, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0068646/'),
(542, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0060196/'),
(541, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0052357/'),
(540, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', '5', 'http://www.imdb.com/title/tt0050083/'),
(539, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0382932/'),
(538, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '4', 'http://www.imdb.com/title/tt0325980/'),
(537, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt1201607/'),
(536, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0892769/'),
(535, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0198781/'),
(534, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0848228/'),
(533, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0114709/'),
(532, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt1049413/'),
(531, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '4', 'http://www.imdb.com/title/tt0910970/'),
(530, 'http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', '5', 'http://www.imdb.com/title/tt0435761/'),
(529, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt1659337/'),
(528, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0454876/'),
(527, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0246578/'),
(526, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '1', 'http://www.imdb.com/title/tt0266697/'),
(525, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0086250/'),
(524, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0361748/'),
(523, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0062622/'),
(522, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0086879/'),
(521, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0095765/'),
(520, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt1675434/'),
(519, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0405094/'),
(518, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0066921/'),
(517, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt1853728/'),
(516, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '3', 'http://www.imdb.com/title/tt0209144/'),
(515, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '3', 'http://www.imdb.com/title/tt0054215/'),
(514, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0102926/'),
(513, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0167261/'),
(512, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0133093/'),
(511, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0109830/'),
(510, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '3', 'http://www.imdb.com/title/tt0120737/'),
(509, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0137523/'),
(508, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '4', 'http://www.imdb.com/title/tt0167260/'),
(507, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', '5', 'http://www.imdb.com/title/tt0110912/'),
(506, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0903624/'),
(505, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '3', 'http://www.imdb.com/title/tt0758758/'),
(504, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0372784/'),
(503, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0110357/'),
(502, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0338013/'),
(501, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '4', 'http://www.imdb.com/title/tt1675434/'),
(500, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '3', 'http://www.imdb.com/title/tt0435761/'),
(499, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt1345836/'),
(498, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0137523/'),
(497, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', '5', 'http://www.imdb.com/title/tt0468569/'),
(496, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt0120382/'),
(495, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt0405094/'),
(494, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt0050083/'),
(493, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '5', 'http://www.imdb.com/title/tt0068646/'),
(492, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt0120737/'),
(491, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt0133093/'),
(490, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '5', 'http://www.imdb.com/title/tt0110357/'),
(489, 'http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', '4', 'http://www.imdb.com/title/tt1375666/'),
(488, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0114369/'),
(487, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0114814/'),
(486, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0317248/'),
(485, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0109830/'),
(484, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0076759/'),
(483, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '3', 'http://www.imdb.com/title/tt0080684/'),
(482, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '3', 'http://www.imdb.com/title/tt0137523/'),
(481, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '3', 'http://www.imdb.com/title/tt0167260/'),
(480, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0468569/'),
(479, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt1375666/'),
(478, 'http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', '5', 'http://www.imdb.com/title/tt0110912/'),
(477, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '5', 'http://www.imdb.com/title/tt0266543/'),
(476, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '3', 'http://www.imdb.com/title/tt0167404/'),
(475, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '5', 'http://www.imdb.com/title/tt0910970/'),
(474, 'http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', '5', 'http://www.imdb.com/title/tt0435761/'),
(473, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0086250/'),
(472, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0110413/'),
(471, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0070511/'),
(470, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0083658/'),
(469, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0075314/'),
(468, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0102926/'),
(467, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0133093/'),
(466, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0099685/'),
(465, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0111161/'),
(464, 'http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', '5', 'http://www.imdb.com/title/tt0068646/'),
(463, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '4', 'http://www.imdb.com/title/tt0120382/'),
(462, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'http://www.imdb.com/title/tt0482571/'),
(461, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'http://www.imdb.com/title/tt0088763/'),
(460, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'http://www.imdb.com/title/tt0245429/'),
(459, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'http://www.imdb.com/title/tt0133093/'),
(458, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', '5', 'http://www.imdb.com/title/tt1375666/'),
(457, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '5', 'http://www.imdb.com/title/tt0172495/'),
(456, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'http://www.imdb.com/title/tt0198781/'),
(455, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'http://www.imdb.com/title/tt0440963/'),
(454, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '5', 'http://www.imdb.com/title/tt0167404/'),
(453, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '1', 'http://www.imdb.com/title/tt0062622/'),
(452, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '4', 'http://www.imdb.com/title/tt0910970/'),
(451, 'http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', '5', 'http://www.imdb.com/title/tt0133093/'),
(450, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '4', 'http://www.imdb.com/title/tt0108052/'),
(449, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '5', 'http://www.imdb.com/title/tt0110912/'),
(448, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', '5', 'http://www.imdb.com/title/tt0068646/'),
(447, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt1675434/'),
(446, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '2', 'http://www.imdb.com/title/tt1375666/'),
(445, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt1220719/'),
(444, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt1049413/'),
(443, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0910970/'),
(442, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'http://www.imdb.com/title/tt0903624/'),
(441, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0892769/'),
(440, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0848228/'),
(439, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0435761/'),
(438, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'http://www.imdb.com/title/tt0382932/'),
(437, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '3', 'http://www.imdb.com/title/tt0319061/'),
(436, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0266543/'),
(435, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'http://www.imdb.com/title/tt0253474/'),
(434, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '5', 'http://www.imdb.com/title/tt0245429/'),
(433, 'http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', '4', 'http://www.imdb.com/title/tt0198781/'),
(432, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '4', 'http://www.imdb.com/title/tt0068646/'),
(431, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '4', 'http://www.imdb.com/title/tt0110912/'),
(430, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '3', 'http://www.imdb.com/title/tt0093058/'),
(429, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'http://www.imdb.com/title/tt0317248/'),
(428, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '4', 'http://www.imdb.com/title/tt1049413/'),
(427, 'http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', '5', 'http://www.imdb.com/title/tt0111161/'),
(426, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'http://www.imdb.com/title/tt0071853/'),
(425, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '3', 'http://www.imdb.com/title/tt0112573/'),
(424, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'http://www.imdb.com/title/tt0076759/'),
(423, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'http://www.imdb.com/title/tt1375666/'),
(422, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '4', 'http://www.imdb.com/title/tt0137523/'),
(421, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'http://www.imdb.com/title/tt0080684/'),
(420, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '5', 'http://www.imdb.com/title/tt0110912/'),
(419, 'http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', '4', 'http://www.imdb.com/title/tt0068646/'),
(418, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '3', 'http://www.imdb.com/title/tt0075314/'),
(417, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'http://www.imdb.com/title/tt0109830/'),
(416, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '4', 'http://www.imdb.com/title/tt0070047/'),
(415, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'http://www.imdb.com/title/tt0090605/'),
(414, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'http://www.imdb.com/title/tt0088763/'),
(413, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'http://www.imdb.com/title/tt0167260/'),
(412, 'http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', '5', 'http://www.imdb.com/title/tt0068646/'),
(411, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0080684/'),
(410, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0076759/'),
(409, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0086190/'),
(408, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0167260/'),
(407, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0167261/'),
(406, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0120737/'),
(405, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '5', 'http://www.imdb.com/title/tt0071853/'),
(404, 'http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', '4', 'http://www.imdb.com/title/tt0110912/'),
(403, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt1049413/'),
(402, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0361748/'),
(401, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0095016/'),
(400, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt1675434/'),
(399, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0066921/'),
(398, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt1853728/'),
(397, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0120586/'),
(396, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0071562/'),
(395, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0068646/'),
(394, 'http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', '5', 'http://www.imdb.com/title/tt0111161/'),
(393, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0434409/'),
(392, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0361748/'),
(391, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0211915/'),
(390, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'http://www.imdb.com/title/tt0120689/'),
(389, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0910970/'),
(388, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'http://www.imdb.com/title/tt0209144/'),
(387, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0110413/'),
(386, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0114369/'),
(385, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0109830/'),
(384, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'http://www.imdb.com/title/tt1375666/'),
(383, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'http://www.imdb.com/title/tt0120737/'),
(382, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0137523/'),
(381, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '5', 'http://www.imdb.com/title/tt0468569/'),
(380, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '4', 'http://www.imdb.com/title/tt0110912/'),
(379, 'http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', '3', 'http://www.imdb.com/title/tt0071562/'),
(378, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0096283/'),
(377, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0434409/'),
(376, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0108052/'),
(375, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0338013/'),
(374, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0137523/'),
(373, 'http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', '5', 'http://www.imdb.com/title/tt0033467/'),
(372, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '4', 'http://www.imdb.com/title/tt0068646/'),
(371, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '5', 'http://www.imdb.com/title/tt0137523/'),
(370, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', '5', 'http://www.imdb.com/title/tt0066921/'),
(369, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '3', 'http://www.imdb.com/title/tt0401792/'),
(368, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '4', 'http://www.imdb.com/title/tt0110912/'),
(367, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '5', 'http://www.imdb.com/title/tt0266697/'),
(366, 'http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', '5', 'http://www.imdb.com/title/tt0133093/'),
(635, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'https://www.imdb.com/title/tt0062622/'),
(636, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://www.imdb.com/title/tt0066921/'),
(637, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'https://www.imdb.com/title/tt0070047/'),
(638, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://www.imdb.com/title/tt0071562/'),
(639, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://www.imdb.com/title/tt0071853/'),
(640, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'https://www.imdb.com/title/tt0075148/'),
(641, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://www.imdb.com/title/tt0076759/'),
(642, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'https://www.imdb.com/title/tt0080684/'),
(643, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '4', 'http://www.imdb.com/title/tt0086879/'),
(644, 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', '5', 'http://www.imdb.com/title/tt0110912/'),
(645, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://www.imdb.com/title/tt0062622/'),
(646, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://www.imdb.com/title/tt0066921/'),
(647, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://www.imdb.com/title/tt0070047/'),
(648, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '3', 'https://www.imdb.com/title/tt0071562/'),
(649, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://www.imdb.com/title/tt0075148/'),
(650, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://www.imdb.com/title/tt0076759/'),
(651, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://www.imdb.com/title/tt0078748/'),
(652, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'https://www.imdb.com/title/tt0080684/'),
(653, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '4', 'https://www.imdb.com/title/tt0090605/'),
(654, 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', '5', 'http://www.imdb.com/title/tt0110912/\r\n'),
(655, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://www.imdb.com/title/tt0062622/'),
(656, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://www.imdb.com/title/tt0070047/'),
(657, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'https://www.imdb.com/title/tt0071562/'),
(658, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://www.imdb.com/title/tt0075148/'),
(659, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'https://www.imdb.com/title/tt0076759/'),
(660, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '4', 'https://www.imdb.com/title/tt0080684/'),
(661, 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', '5', 'http://www.imdb.com/title/tt0097576/'),
(662, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0078748/'),
(663, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0080684/'),
(664, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0083658/'),
(665, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0110912/'),
(666, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0111161/'),
(667, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '4', 'http://www.imdb.com/title/tt0114709/'),
(668, 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', '5', 'http://www.imdb.com/title/tt0167261/');

-- --------------------------------------------------------

--
-- Stand-in structure for view `le1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `le1`;
CREATE TABLE IF NOT EXISTS `le1` (
`login` varchar(100)
,`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `le2`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `le2`;
CREATE TABLE IF NOT EXISTS `le2` (
`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
CREATE TABLE IF NOT EXISTS `pessoa` (
  `login` varchar(100) NOT NULL,
  `nome_completo` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `nascimento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`login`, `nome_completo`, `cidade`, `nascimento`) VALUES
('http://utfpr.edu.br/CSB30/2018/1/DI1801alefedias', 'Alefe Felipe Goncalves Pereira Dias', 'Araucária', '1998-06-02'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801alexmatsuo', 'Alex Matsuo', 'Curitiba', '1996-04-04'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801brunoguillen', 'Bruno Guillen', 'Curitiba', '1992-10-17'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiosordi', 'Caio Hurtado Sordi', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801caiquedestro', 'Caique Destro', 'Curitiba', '1996-08-03'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801carlosjunior', 'Carlos Roberto Remenche Junior', 'Curitiba', '1987-12-03'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801cesarbatista', 'Cesar Batista', 'Curitiba', '1995-05-05'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801clevertoncarneiro', 'Cleverton Wellinton Ferreira Carneiro', 'São José dos Campos', '1996-03-28'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801eduardootte', 'Eduardo Kirsten Otte', 'Curitiba', '1997-11-24'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801enricomanfron', 'Enrico Manfron', 'curitiba', '1997-04-15'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801erikyamamoto', 'Erik Ryuichi Yamamoto', 'Ichihara', '1996-02-01'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandosilva', 'Fernando Argentino Da Silva', 'Curitiba', '1996-07-06'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis', 'Fernando Levy Silvestre De Assis', 'Curitiba', '1992-08-21'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801filipesilva', 'Filipe De Freitas Martins Da Silva', 'Curitiba', '1994-12-05'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes', 'Gabriel Pena Peres', 'Curitiba', '1995-01-21'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801gustavopupo', 'Gustavo Daniel Pupo', 'Ponta Grossa', '1994-12-23'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801hamiltonneto', 'Hamilton Alves Mendes Neto', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801henriquecastro', 'Henrique Hirabara De Castro', 'Bandeirantes', '1997-11-04'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joaoneto', 'Joao Calixto Bonfim Neto', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto', 'Jose De Faria Leite Neto', 'Curitiba', '1995-10-03'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801josemoreno', 'Jose Vitor Moreno', 'Quatro Barras', '1997-08-06'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801juanmansano', 'Juan Simao Mansano', 'Pinhais', '1994-07-05'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801julianacorrea', 'Juliana Correa', 'Miranda - MS', '1987-08-05'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucasrech', 'Lucas Coradin Rech', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassilva', 'Lucas Patrick Da Silva', 'Curitiba', '1995-09-29'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801lucassouza', 'Lucas Ricardo Marques De Souza', 'Curitiba', '1997-07-13'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luccaholosbach', 'Lucca Rawlyk Holosbach', 'Curitiba', '1997-11-11'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luisjunior', 'Luis Vala Junior', 'Ortigueira - PR', '1997-04-06'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801luizagner', 'Luiz Rafael Do Nascimento Agner', 'Curitiba', '1996-05-07'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcellepires', 'Marcelle Reis Pires', 'Cachoeiro de Itapemirim', '1997-02-16'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801marcusmorais', 'Marcus Vinicius Reis De Morais', 'Curitiba', '1997-03-26'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mariapinho', 'Maria Eduarda Rebelo Pinho', 'Belém', '1997-12-06'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801mateusgomes', 'Mateus Pereira Gomes', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801matheusdias', 'Matheus Giovanni Dias', 'São Paulo', '1997-12-04'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801natanjunges', 'Natan De Almeida Junges', 'Curitiba', '1998-05-11'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801nicolasabril', 'Nicolas Abril', 'Curitiba', '1997-05-14'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801patrikymirkoski', 'Patriky Edeovan Galvao Mirkoski', 'Prudentópolis', '1992-05-10'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801pedroneto', 'Pedro Bazia Neto', 'Curitiba', '1993-04-29'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801ricardoardissone', 'Ricardo Luis Souza Ardissone', '', ''),
('http://utfpr.edu.br/CSB30/2018/1/DI1801rodrigosilva', 'Rodrigo Faria Santos Da Silva', 'Curitiba', '1998-10-06'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801victorlourenco', 'Victor Feitosa Lourenco', 'Sao Paulo', '1998-03-24'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801viniciusbernert', 'Vinicius Debur Bernert', 'Curitiba', '1995-04-05'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin', 'Wilian Henrique Cavassin', 'Curitiba', '1992-12-19'),
('http://utfpr.edu.br/CSB30/2018/1/DI1801williamsouza', 'William De Oliveira Souza', 'Piraquara', '1995-03-28');

-- --------------------------------------------------------

--
-- Stand-in structure for view `we1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `we1`;
CREATE TABLE IF NOT EXISTS `we1` (
`login` varchar(100)
,`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `we2`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `we2`;
CREATE TABLE IF NOT EXISTS `we2` (
`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ze1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `ze1`;
CREATE TABLE IF NOT EXISTS `ze1` (
`login` varchar(100)
,`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ze2`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `ze2`;
CREATE TABLE IF NOT EXISTS `ze2` (
`login2` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `conhecenormalizada`
--
DROP TABLE IF EXISTS `conhecenormalizada`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `conhecenormalizada`  AS  (select distinct `uniao`.`login` AS `login`,`uniao`.`login2` AS `login2` from (select `contato`.`login` AS `login`,`contato`.`login2` AS `login2` from `contato` union select `contato`.`login2` AS `login2`,`contato`.`login` AS `login` from `contato`) `uniao` order by `uniao`.`login`) ;

-- --------------------------------------------------------

--
-- Structure for view `ge1`
--
DROP TABLE IF EXISTS `ge1`;
-- Error reading structure for table t1_alfa.ge1: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `ge2`
--
DROP TABLE IF EXISTS `ge2`;
-- Error reading structure for table t1_alfa.ge2: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `le1`
--
DROP TABLE IF EXISTS `le1`;
-- Error reading structure for table t1_alfa.le1: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `le2`
--
DROP TABLE IF EXISTS `le2`;
-- Error reading structure for table t1_alfa.le2: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `we1`
--
DROP TABLE IF EXISTS `we1`;
-- Error reading structure for table t1_alfa.we1: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `we2`
--
DROP TABLE IF EXISTS `we2`;
-- Error reading structure for table t1_alfa.we2: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `ze1`
--
DROP TABLE IF EXISTS `ze1`;
-- Error reading structure for table t1_alfa.ze1: #1046 - Nenhum banco de dados foi selecionado

-- --------------------------------------------------------

--
-- Structure for view `ze2`
--
DROP TABLE IF EXISTS `ze2`;
-- Error reading structure for table t1_alfa.ze2: #1046 - Nenhum banco de dados foi selecionado
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
