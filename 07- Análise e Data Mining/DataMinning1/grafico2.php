<?php
//CONEXÃO DO BANCO LOCAL
$pcon = new mysqli("localhost", "root", "", "t1_alfa");
$SQL8A = "SELECT login, COUNT(*) AS freq2 FROM gostafilme GROUP BY login ORDER BY COUNT(*) DESC LIMIT 30";
$resultado8A = mysqli_query($pcon, $SQL8A);
$chartdata8A = array();
$chartdata8B = array();
$cor2 = array();
$cor2[0] = '#111155';
$i=0;
while ($rows = mysqli_fetch_object($resultado8A)) {
        $login = $rows->login;
        $count = $rows->freq2;
        $chartdata8A[$i] = $login;
        $chartdata8B[$i] = $count;
        $i = $i + 1;        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Gráfico 2</title>      
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>  
    <script type="text/javascript">
        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Pessoas", "Filmes Curtidos", {role: "style"}],
                <?php
                $k = $i;
                for($i=0; $i < $k; $i++){
                ?>
                    ['<?php echo $chartdata8A[$i] ?>', <?php echo $chartdata8B[$i] ?>,'<?php echo $cor2[0] ?>'],
                <?php } ?>
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"},
                2]);

            var options = {
                title: "Filmes curtidos por pessoa",
                width: 800,
                height: 500,
                bar: {groupWidth: "95%"},
                legend: {position: "none"},
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }
    </script>
    <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
    <body>
    </body>
</html>