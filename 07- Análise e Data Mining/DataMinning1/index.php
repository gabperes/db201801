<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <title>TRABALHO 6 - ANALISE E DATA MINNING</title>
    </head>
    <body>
            <?php
            //CONEXÃO DO BANCO LOCAL
            $pcon = new mysqli("localhost", "root", "", "t1_alfa");
            if (mysqli_connect_errno()) {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }
            ?>
        <h1>Autores: Wilian Cavassin, José Neto, Gabriel Peres, Fernando Levy</h1>    
        <p1>Média e STD para Artistas Musicais: <br></p1>
        <p2>Média:<br></p2>
            <?php
            $SQL1A = 'SELECT AVG(avaliacao)FROM gostaartista';
            $resultado1A = $pcon->query($SQL1A) OR trigger_error($pcon->error, E_USER_ERROR);
            while ($rows = $resultado1A->fetch_assoc()) {
                print_r($rows);
            }
            $resultado1A->free();
            ?>
        <p2><br></p2>
        <p2>Desvio Padrão:<br></p2>
            <?php
            $SQL1B = 'SELECT STD(avaliacao)FROM gostaartista';
            $resultado1B = $pcon->query($SQL1B) OR trigger_error($pcon->error, E_USER_ERROR);
            while ($rows = $resultado1B->fetch_assoc()) {
                print_r($rows);
            }
            $resultado1B->free();
            ?>
        <p2><br></p2>
        <p1>Média e STD para Filmes:<br></p1>
        <p2>Média:<br></p2>
            <?php
            $SQL1C = 'SELECT AVG(avaliacao)FROM gostafilme';
            $resultado1C = $pcon->query($SQL1C) OR trigger_error($pcon->error, E_USER_ERROR);
            while ($rows = $resultado1C->fetch_assoc()) {
                print_r($rows);
            }
            $resultado1C->free();
            ?>
        <p2><br></p2>
        <p2>Desvio Padrão:<br></p2>
            <?php
            $SQL1D = 'SELECT STD(avaliacao)FROM gostafilme';
            $resultado1D = $pcon->query($SQL1D) OR trigger_error($pcon->error, E_USER_ERROR);
            while ($rows = $resultado1D->fetch_assoc()) {
                print_r($rows);
            }
            $resultado1D->free();
            ?>
        <p2><br><br></p2>
        <p2>Média do rating de artistas curtidos por mais de uma pessoa</br></p2>
            <?php
            $SQL2A = 'SELECT idart, AVG(avaliacao) FROM gostaartista GROUP BY idart, avaliacao
            HAVING COUNT(idart) >= 2 ORDER BY AVG(avaliacao) DESC';
            $resultado2A = $pcon->query($SQL2A) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado2A)) {
                echo"<tr><td>{$rows['idart']}</td><td>{$rows['AVG(avaliacao)']}</td></tr>";
            }           
            echo"</table>"
            ?>
        <p2><br><br></p2>
        <p2>TOP 10<br></p2>
        <p2>Artistas Musicais<br></p2>
            <?php
            $SQL3A = 'select idart, COUNT(*) AS frequencia FROM gostaartista GROUP BY idart
            ORDER BY frequencia DESC LIMIT 10';
            $resultado3A = $pcon->query($SQL3A) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado3A)) {
                echo"<tr><td>{$rows['idart']}</td><td>{$rows['frequencia']}</td></tr>";
            }
            echo"</table>";
            $resultado3A->free();
            ?>
        <p2>Filmes<br></p2>
            <?php
            $SQL3B = 'select idart, COUNT(*) AS frequencia FROM gostafilme GROUP BY idart
            ORDER BY frequencia DESC LIMIT 10';
            $resultado3B = $pcon->query($SQL3B) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado3B)) {
                echo"<tr><td>{$rows['idart']}</td><td>{$rows['frequencia']}</td></tr>";
            }
            echo"</table>";
            $resultado3B->free();
            ?>
        <p2><br></p2>
        <p2>Conhece Normalizada<br></p2>
            <?php
            $SQL4A = 'CREATE OR REPLACE VIEW conhecenormalizada AS(SELECT DISTINCT * FROM
            (SELECT login, login2 FROM contato UNION SELECT login2, login FROM contato) AS uniao ORDER by login)';
            $resultado4A = $pcon->query($SQL4A) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL4B = 'SELECT * FROM conhecenormalizada';
            $resultado4B = $pcon->query($SQL4B) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado4B)) {
                echo"<tr><td>{$rows['login']}</td><td>{$rows['login2']}</td></tr>";
            }
            echo"</table>";
            $resultado4B->free();
            ?>
        <p2><br></p2>
        <p2>Conhecendo Filmes em comun por quantidade<br></p2>
            <?php
            $SQL5A = 'SELECT a.login, a.login2, count(*) FROM gostafilme b, gostafilme c, conhecenormalizada a WHERE b.login = a.login AND c.login = a.login2 AND b.idart = c.idart GROUP BY a.login, a.login2 ORDER BY `count(*)` DESC';
            $resultado5A = $pcon->query($SQL5A) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado5A)) {
                echo"<tr><td>{$rows['login']}</td><td>{$rows['login2']}</td><td>{$rows['count(*)']}</td></tr>";
            }
            echo"</table>";
            $resultado5A->free();
            ?>
        <p2><br></p2>
        <p2>Numero de conhecidos de conhecidos do grupo<br></p2>
        <p2>Zézão<br></p2>
            <?php
            $SQL6ZA = "CREATE OR REPLACE VIEW ze1 AS(SELECT login, login2 FROM conhecenormalizada WHERE login = 'http://utfpr.edu.br/CSB30/2018/1/DI1801joseneto%27')";
            $resultado6ZA = $pcon->query($SQL6ZA) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6ZB = 'CREATE OR REPLACE VIEW ze2 AS (SELECT z.login2 FROM conhecenormalizada c, ze1 z WHERE z.login2 = c.login)';
            $resultado6ZB = $pcon->query($SQL6ZB) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6ZC = 'SELECT login2, count(*) FROM ze2 GROUP BY login2';
            $resultado6ZC = $pcon->query($SQL6ZC) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado6ZC)) {
                echo"<tr><td>{$rows['login2']}</td><td>{$rows['count(*)']}</td></tr>";
            }
            echo"</table>";
            $resultado6ZC->free();
            ?>
        <p2><br></p2>
        <p2>Levy<br></p2>
            <?php
            $SQL6LA = "CREATE OR REPLACE VIEW le1 AS(SELECT login, login2 FROM conhecenormalizada WHERE login = 'http://utfpr.edu.br/CSB30/2018/1/DI1801fernandoassis')";
            $resultado6LA = $pcon->query($SQL6LA) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6LB = 'CREATE OR REPLACE VIEW le2 AS (SELECT l.login2 FROM conhecenormalizada c, le1 l WHERE l.login2 = c.login)';
            $resultado6LB = $pcon->query($SQL6LB) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6LC = 'SELECT login2, count(*) FROM le2 GROUP BY login2';
            $resultado6LC = $pcon->query($SQL6LC) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado6LC)) {
                echo"<tr><td>{$rows['login2']}</td><td>{$rows['count(*)']}</td></tr>";
            }
            echo"</table>";
            $resultado6LC->free();
            ?>
        <p2><br></p2>
        <p2>Gabriel<br></p2>
            <?php
            $SQL6GA = "CREATE OR REPLACE VIEW ge1 AS(SELECT login, login2 FROM conhecenormalizada WHERE login = 'http://utfpr.edu.br/CSB30/2018/1/DI1801gabrielperes')";
            $resultado6GA = $pcon->query($SQL6GA) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6GB = 'CREATE OR REPLACE VIEW ge2 AS (SELECT g.login2 FROM conhecenormalizada c, ge1 g WHERE g.login2 = c.login)';
            $resultado6GB = $pcon->query($SQL6GB) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6GC = 'SELECT login2, count(*) FROM ge2 GROUP BY login2';
            $resultado6GC = $pcon->query($SQL6GC) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'"; 
            while ($rows = mysqli_fetch_assoc($resultado6GC)) {
                echo"<tr><td>{$rows['login2']}</td><td>{$rows['count(*)']}</td></tr>";
            }
            echo"</table>";
            $resultado6GC->free();
            ?>
        <p2><br></p2>
        <p2>Cavassin<br></p2>
            <?php
            $SQL6WA = "CREATE OR REPLACE VIEW we1 AS(SELECT login, login2 FROM conhecenormalizada WHERE login = 'http://utfpr.edu.br/CSB30/2018/1/DI1801wiliancavassin')";
            $resultado6WA = $pcon->query($SQL6WA) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6WB = 'CREATE OR REPLACE VIEW we2 AS (SELECT w.login2 FROM conhecenormalizada c, we1 w WHERE w.login2 = c.login)';
            $resultado6WB = $pcon->query($SQL6WB) OR trigger_error($pcon->error, E_USER_ERROR);
            $SQL6WC = 'SELECT login2, count(*) FROM we2 GROUP BY login2';
            $resultado6WC = $pcon->query($SQL6WC) OR trigger_error($pcon->error, E_USER_ERROR);
            echo "<table border = '2'";
            while ($rows = mysqli_fetch_assoc($resultado6WC)) {
                echo"<tr><td>{$rows['login2']}</td><td>{$rows['count(*)']}</td></tr>";
            }
            echo"</table>";
            $resultado6WC->free();
            ?>
        <p2><br></p2>
        <!--GRAFICOS-->
        <p2>Gráficos<br></p2>
        <a href="grafico1.php">Grafico 1</a>
        <p2><br></p2>
        <a href="grafico2.php">Grafico 2</a>
        <p2><br></p2>
        <p2>Curitibanos:<br></p2>
            <?php
            $SQL9A = "SELECT COUNT(cidade) FROM pessoa WHERE cidade LIKE 'Curitiba%'";
            $resultado9A = $pcon->query($SQL9A) OR trigger_error($pcon->error, E_USER_ERROR);
            while ($rows = $resultado9A->fetch_assoc()) {
            print_r($rows);
            }
            ?>
        <p2><br></p2>
        <p2>Pessoas mais populares:<br></p2>
            <?php
            $SQL10A = 'SELECT login, COUNT(*) FROM contato GROUP BY login ORDER BY COUNT(*) DESC LIMIT 10';
            $resultado10A = $pcon->query($SQL10A) OR trigger_error($pcon->error, E_USER_ERROR);
            echo"<table border = 2>";
            while ($rows = mysqli_fetch_assoc($resultado10A)) {
                echo"<tr><td>{$rows['login']}</td><td>{$rows['COUNT(*)']}";
            }
            echo"</table>";
            $resultado10A->free();
            ?>
        <p2><br></p2>
    </body>
</html>
