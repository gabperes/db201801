<?php
//CONEXÃO DO BANCO LOCAL
$pcon = new mysqli("localhost", "root", "", "t1_alfa");
$SQL7A = "SELECT idart, COUNT(*) AS freq FROM gostafilme GROUP BY idart ORDER BY COUNT(*) DESC LIMIT 30";
$resultado7A = mysqli_query($pcon, $SQL7A);
$chartdata7A = array();
$chartdata7B = array();
$cor = array();
$cor[0] = '#115511';
$i=0;
while ($rows = mysqli_fetch_object($resultado7A)) {
        $idart = $rows->idart;
        $count = $rows->freq;
        $chartdata7A[$i] = $idart;
        $chartdata7B[$i] = $count;
        $i = $i + 1;        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Gráfico 1</title>       
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>  
    <script type="text/javascript">
        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Filmes", "Curtidas", {role: "style"}],
                <?php
                $k = $i;
                for($i=0; $i < $k; $i++){
                ?>
                    ['<?php echo $chartdata7A[$i] ?>', <?php echo $chartdata7B[$i] ?>,'<?php echo $cor[0] ?>'],
                <?php } ?>
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"},
                2]);

            var options = {
                title: "Pessoas por Filmes Curtidos",
                width: 800,
                height: 500,
                bar: {groupWidth: "95%"},
                legend: {position: "none"},
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }
    </script>
    <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
    <body>
    </body>
</html>