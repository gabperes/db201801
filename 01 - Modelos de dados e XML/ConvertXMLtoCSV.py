#!/usr/bin/python
from xml.dom.minidom import parse
import xml.dom.minidom
import csv
import os
# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")
universe = DOMTree.documentElement
if universe.hasAttribute("name"):
   print("Root element : %s" % universe.getAttribute("name"))
# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

arquivo1 = open('.\dadosMarvel\herois_good.csv', 'w')
arquivo2 = open('.\dadosMarvel\herois_bad.csv', 'w')
wr1 = csv.writer(arquivo1, quoting=csv.QUOTE_ALL)
wr2 = csv.writer(arquivo2, quoting=csv.QUOTE_ALL)
#writerow = escreve uma linha - separacao de colunas e feita po virgula
atributos = ["ID","Name","Popularity","Alignment","Gender","Height_M","Weight_Kg","Hometown","Intelligence","Strenght","Speed","Durability","Energy_Projection","Fighting Skills"]
for atributo in atributos:
    wr1.writerow([atributo])
    wr2.writerow([atributo])
# Print detail of each hero.
good = 0.0
bad = 0.0
peso = 0
herois = 0
for hero in heroes:
   name = hero.getElementsByTagName('name')[0]
   popularity = hero.getElementsByTagName("popularity")[0]
   alignment = hero.getElementsByTagName("alignment")[0]
   gender = hero.getElementsByTagName("gender")[0]
   height_m = hero.getElementsByTagName("height_m")[0]
   weight_kg = hero.getElementsByTagName("weight_kg")[0]
   hometown = hero.getElementsByTagName("hometown")[0]
   intelligence = hero.getElementsByTagName("intelligence")[0]
   strength = hero.getElementsByTagName("strength")[0]
   speed = hero.getElementsByTagName("speed")[0]
   durability = hero.getElementsByTagName("durability")[0]
   energy_projection = hero.getElementsByTagName("energy_Projection")[0]
   skills = hero.getElementsByTagName("fighting_Skills")[0]
   if alignment.childNodes[0].data == "Good" or alignment.childNodes[0].data == "Neutral":
    good += 1
    wr1.writerow([hero.getAttribute("id"),name.childNodes[0].data,popularity.childNodes[0].data,alignment.childNodes[0].data,gender.childNodes[0].data,height_m.childNodes[0].data,weight_kg.childNodes[0].data,hometown.childNodes[0].data,intelligence.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy_projection.childNodes[0].data,skills.childNodes[0].data])
   else:
    bad += 1
    wr2.writerow([hero.getAttribute("id"),name.childNodes[0].data,popularity.childNodes[0].data,alignment.childNodes[0].data,gender.childNodes[0].data,height_m.childNodes[0].data,weight_kg.childNodes[0].data,hometown.childNodes[0].data,intelligence.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy_projection.childNodes[0].data,skills.childNodes[0].data])
   peso += int(weight_kg.childNodes[0].data)
   herois += 1
print ("Para cada heroi bom, existem %f herois maus" % float(good/bad))
print ("Peso medio dos herois: %f" % float(peso/herois))
nome = "Hulk"
for hero in heroes:
    name = hero.getElementsByTagName("name")[0]
    if name.childNodes[0].data == nome:
        weight_kg = hero.getElementsByTagName("weight_kg")[0]
        height_m = hero.getElementsByTagName("height_m")[0]
        print ("IMC do Heroi: %f" % float(int(weight_kg.childNodes[0].data)/(float(height_m.childNodes[0].data)*float(height_m.childNodes[0].data))))

import xml.etree.cElementTree as ET

tree = ET.parse('marvel_simplificado.xml')
root = tree.getroot()

xml_data_to_csv = open('dadosMarvel/herois.csv', 'w', newline="")
list_head = []
Csv_writer = csv.writer(xml_data_to_csv)
count = 0

for element in root.findall('hero'):
    List_nodes = []
    if count == 0:
        name = element.find('name').tag
        list_head.append(name)
        alignment = element.find('alignment').tag
        list_head.append(alignment)
        gender = element.find('gender').tag
        list_head.append(gender)
        height_m = element.find('height_m').tag
        list_head.append(height_m)
        weight_kg = element.find('weight_kg').tag
        list_head.append(weight_kg)
        hometown = element.find('hometown').tag
        list_head.append(hometown)
        intelligence = element.find('intelligence').tag
        list_head.append(intelligence)
        strength = element.find('strength').tag
        list_head.append(strength)
        speed = element.find('speed').tag
        list_head.append(speed)
        durability = element.find('durability').tag
        list_head.append(durability)
        energy_Projection = element.find('energy_Projection').tag
        list_head.append(energy_Projection)
        fighting_Skills = element.find('fighting_Skills').tag
        list_head.append(fighting_Skills)
    #Csv_writer.writerow(list_head)
    count = +1

    #filhos
    name = element.find('name').text
    List_nodes.append(name)
    alignment = element.find('alignment').text
    List_nodes.append(alignment)
    gender = element.find('gender').text
    List_nodes.append(gender)
    height_m = element.find('height_m').text
    List_nodes.append(height_m)
    weight_kg = element.find('weight_kg').text
    List_nodes.append(weight_kg)
    hometown = element.find('hometown').text
    List_nodes.append(hometown)
    intelligence = element.find('intelligence').text
    List_nodes.append(intelligence)
    strength = element.find('strength').text
    List_nodes.append(strength)
    speed = element.find('speed').text
    List_nodes.append(speed)
    durability = element.find('durability').text
    List_nodes.append(durability)
    energy_Projection = element.find('energy_Projection').text
    List_nodes.append(energy_Projection)
    fighting_Skills = element.find('fighting_Skills').text
    List_nodes.append(fighting_Skills)
    Csv_writer.writerow(List_nodes)
    xml_data_to_csv.close
