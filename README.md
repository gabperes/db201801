# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Modifique este aquivo README.md com as informações adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usuário GitLab para cada integrante do grupo.

- Gabriel Pena Peres, 1561928, gabperes
- Wilian Henrique Cavassin, 1245260, Vidgonio
- José de Faria Leite Neto, 1654829, zeneto
- Fernando Levy Silvestre de Assis, 1654799, taracaca

## Descrição da aplicação a ser desenvolvida 

Descreva aqui uma visão geral da aplicação que será desenvolvida pelo grupo durante o semestre. **Este texto deverá ser escrito quando requisitado pelo professor.** O conteúdo vai evoluir à medida em que o grupo avança com a implementação.
