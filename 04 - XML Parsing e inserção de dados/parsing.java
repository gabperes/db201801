import ConnectionFactory.ConnectionFactory ;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class parsing {

    public static void main(String argv[]) {

        try {
            
            Connection connection = new ConnectionFactory().getConnection(); 
            
            URLConnection urlPessoas = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml").openConnection();
            URLConnection urlArtistas = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml").openConnection();
            URLConnection urlFilmes = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml").openConnection();
            URLConnection urlConhecidos = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml").openConnection();

 
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            //insere Pessoas
            InputStream stream = urlPessoas.getInputStream();
            org.w3c.dom.Document doc = dBuilder.parse(stream); 
            NodeList nList = doc.getElementsByTagName("Person");
            doc.getDocumentElement().normalize(); 

            
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode; 

                PreparedStatement query = connection.prepareStatement("SELECT * FROM pessoa WHERE login = "
                        + "'" + eElement.getAttribute("uri") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO pessoa VALUES('"
                            + eElement.getAttribute("uri")
                            + "','" + eElement.getAttribute("name")
                            + "','" + eElement.getAttribute("hometown")
                            + "','" + eElement.getAttribute("birthdate")+ "');");

                    stmt.execute(); 
                    stmt.close();
                }
            }
            
          
            //insere em Artista
            stream = urlArtistas.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("LikesMusic");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                //insere artistas
                PreparedStatement query = connection.prepareStatement("SELECT * FROM artistas_musicais WHERE uria = "
                        + "'" + eElement.getAttribute("bandUri") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO artistas_musicais VALUES('"
                           
                    + eElement.getAttribute("bandUri") + "');");
                    stmt.execute();
                    stmt.close();
                }
            }
            //insere em GostaArtista
         
           // nList = doc.getElementsByTagName("LikesMusic");
            doc.getDocumentElement().normalize();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode; 

                PreparedStatement query = connection.prepareStatement("SELECT * FROM gostaartista WHERE login = "
                        + "'" + eElement.getAttribute("person") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO gostaartista VALUES('"
                            + eElement.getAttribute("person")
                            + "','" + eElement.getAttribute("rating")
                            + "','" + eElement.getAttribute("bandUri") + "');");

                    stmt.execute(); 
                    stmt.close();
                }
            }

            //insere em GostaFilme
            stream = urlFilmes.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("LikesMovie");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                
                //insere filme
                PreparedStatement query = connection.prepareStatement("SELECT * FROM filmes WHERE "
                        + "uriF='"+eElement.getAttribute("movieUri")+"';");
                ResultSet rs = query.executeQuery();
                 
                if (!rs.next()){
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO filmes VALUES('"
                        + eElement.getAttribute("movieUri") + "');");
                stmt.execute();
                stmt.close();
                }
            }     
           //insere em GostaFilmes
            
            nList = doc.getElementsByTagName("LikesMovie");
            doc.getDocumentElement().normalize();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode; 

                PreparedStatement query = connection.prepareStatement("SELECT * FROM gostafilme WHERE login = "
                        + "'" + eElement.getAttribute("person") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO gostafilme VALUES('"
                            + eElement.getAttribute("person")
                            + "','" + eElement.getAttribute("rating")
                            + "','" + eElement.getAttribute("movieUri") + "');");

                    stmt.execute(); 
                    stmt.close();
                }
            }
            
            //insere contatos
            stream = urlConhecidos.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("Knows");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                PreparedStatement query = connection.prepareStatement("SELECT * FROM contato WHERE"
                        + " login= '"+ eElement.getAttribute("person")+"'"
                        + "AND login2= '" + eElement.getAttribute("colleague") + "';");
                ResultSet rs = query.executeQuery();
                if (!rs.next()){
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO contato VALUES('"
                        + eElement.getAttribute("person")
                        + "','" + eElement.getAttribute("colleague")
                        + "');");
                stmt.execute();
                stmt.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}