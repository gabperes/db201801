CREATE TABLE Pessoa(
    login VARCHAR(100) NOT NULL,
    nome_completo VARCHAR(90),
    cidade VARCHAR(90),
    nascimento VARCHAR(10),
    PRIMARY KEY(login)
);

CREATE TABLE Artistas_Musicais(
    login VARCHAR(100) NOT NULL,
    avaliacaoM VARCHAR(10),
    uriA VARCHAR(100),
    PRIMARY KEY(login, uriA),
    FOREIGN KEY(login),
	REFERENCES Pessoa(login)
);


CREATE TABLE Filmes(
    login VARCHAR(100) NOT NULL,
    avaliacaoF VARCHAR(100),
    uriF VARCHAR(100),
    PRIMARY KEY(login, uriF),
    FOREIGN KEY(login)
	REFERENCES Pessoa(login)
);

CREATE TABLE Contato(
	login VARCHAR(100) NOT NULL,
            login2 VARCHAR(100) NOT NULL,
	PRIMARY KEY(login, login2),
	FOREIGN KEY(login)
		REFERENCES Pessoa(login),
	FOREIGN KEY(login2)
		REFERENCES Pessoa(login),
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);


